package br.com.gitproject.gitproject.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.gitproject.gitproject.R;
import br.com.gitproject.gitproject.Utils.CircleTransformation;
import br.com.gitproject.gitproject.models.RepositoryPull;

/**
 * Created by rcastilho on 28/02/2018.
 */

public class RepositoryPullsListAdapter extends ArrayAdapter<RepositoryPull> {
    private List<RepositoryPull> itens = new ArrayList<RepositoryPull>();
    private Activity activity;
    LayoutInflater inflater;



    public RepositoryPullsListAdapter(Activity activity, List<RepositoryPull> itens) {
        super(activity, R.layout.item_repository_pulls, itens);
        this.inflater = activity.getLayoutInflater();
        this.itens = itens;

        this.activity = activity;
        //view = activity.getLayoutInflater().inflate(R.layout.view_tipo_modus_operandi, null);
    }

    public static class ViewHolder {

        public TextView title;
        public TextView body;
        public ImageView avatar;
        public TextView login;

    }


    public View getView(int position, View convertView, ViewGroup parent) {
        RepositoryPull repositoryPull = itens.get(position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_repository_pulls, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.body = (TextView)convertView.findViewById(R.id.tvBody);
            holder.avatar = (ImageView) convertView.findViewById(R.id.ivAvatar) ;
            holder.login = (TextView) convertView.findViewById(R.id.tvLogin);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }



        holder.title.setText(repositoryPull.getTitle());
        holder.body.setText(repositoryPull.getBody());
        holder.login.setText(repositoryPull.getUser().getLogin());
        Picasso.with(activity).load(repositoryPull.getUser().getAvatar_url()).transform(new CircleTransformation()).into(holder.avatar);



        return convertView;
    }
}