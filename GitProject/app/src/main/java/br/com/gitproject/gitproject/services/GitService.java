package br.com.gitproject.gitproject.services;

import java.util.List;
import java.util.Map;

import br.com.gitproject.gitproject.models.RepositoryPull;
import br.com.gitproject.gitproject.models.RepositoryResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by rcastilho on 05/02/2018.
 */

public interface GitService {
   @GET("search/repositories")
   Call<RepositoryResult> getRepositories(@QueryMap Map<String,String> options);

   @GET("repos/{owner}/{project}/pulls")
   Call<List<RepositoryPull>> getPulls(@Path("owner") String owner, @Path("project") String project);
}
