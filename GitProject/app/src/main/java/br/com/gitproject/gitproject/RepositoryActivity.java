package br.com.gitproject.gitproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.gitproject.gitproject.Adapters.RepositoryPullsListAdapter;
import br.com.gitproject.gitproject.Utils.DetectaConexao;
import br.com.gitproject.gitproject.models.RepositoryPull;
import br.com.gitproject.gitproject.tasks.PullListTask;

public class RepositoryActivity extends BaseActivity {
    String owner;
    String project;
    List<RepositoryPull> repositoryPullsList;
    private ListView lvRepositoryPull;
    SwipeRefreshLayout mySwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);
        checaConexao(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        owner = intent.getStringExtra("owner");
        project = intent.getStringExtra("project");

        setTitle(project);

        repositoryPullsList = new ArrayList<RepositoryPull>();
        lvRepositoryPull = (ListView) findViewById(R.id.repositories_pull_list);
        mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshPulls();
                    }
                }
        );


        mySwipeRefreshLayout.setRefreshing(true);
        refreshPulls();



    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    public void refreshPulls(){
        if(new DetectaConexao(getApplicationContext()).existeConexao()) {

            new PullListTask(new PullListTask.OnRefreshRepositoryPullListener() {

                @Override
                public void resfreshRepository(List<RepositoryPull> repositoryPulls) {
                    repositoryPullsList = repositoryPulls;
                    RepositoryPullsListAdapter adapter = new RepositoryPullsListAdapter(RepositoryActivity.this, repositoryPullsList);
                    lvRepositoryPull.setAdapter(adapter);

                    mySwipeRefreshLayout.setRefreshing(false);

                }
            }, owner, project).execute();
        }
        else{
            Intent intent = new Intent(getApplicationContext(), ErroConexaoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
