package br.com.gitproject.gitproject;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import br.com.gitproject.gitproject.Utils.DetectaConexao;


public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        return true;

    }

    public void checaConexao(Context context) {

        if (!new DetectaConexao(context).existeConexao()) {
            Intent intent = new Intent(context, ErroConexaoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            finish();
        }
    }





    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}

