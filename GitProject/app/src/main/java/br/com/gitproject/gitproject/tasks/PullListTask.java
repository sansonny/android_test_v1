package br.com.gitproject.gitproject.tasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.gitproject.gitproject.BuildConfig;
import br.com.gitproject.gitproject.Utils.Parameters;
import br.com.gitproject.gitproject.models.RepositoryPull;
import br.com.gitproject.gitproject.services.GitService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rcastilho on 05/03/2018.
 */

public class PullListTask extends AsyncTask<Void, Void, List<RepositoryPull>> {


    public List<RepositoryPull> repositoryPulls;
    private OnRefreshRepositoryPullListener listener;
    public String owner;
    public String project;

    public PullListTask(OnRefreshRepositoryPullListener listener, String owner, String project) {
        this.listener = listener;
        this.owner = owner;
        this.project = project;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected List<RepositoryPull> doInBackground(Void... voids){


        /*
        * Olá, como está? Deixei OkHttpclient para você que vai analisar meu código, ter feedback
        * das chamadas e retornos. Eu mesmo me preocupei quando ví que em alguns momentos a lista não chegava até o fim.
        * Isso acontece por causa que estou utilizando a versão não autenticada da api. Como não era um pré-requesito, não a autentiquei.
        * */

        OkHttpClient.Builder okhttpClienteBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        if(BuildConfig.DEBUG){
            okhttpClienteBuilder.addInterceptor(logging);
        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Parameters.URL_REPOSITORIES)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttpClienteBuilder.build());

        final Retrofit retrofit = builder.build();
        GitService service = retrofit.create(GitService.class);
        Call<List<RepositoryPull>> call = service.getPulls(owner,project);

        try {
            repositoryPulls = (ArrayList) new ArrayList<RepositoryPull>();
            repositoryPulls = call.execute().body();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return repositoryPulls;
    }

    @Override
    protected void onPostExecute(List<RepositoryPull> repositoryPulls) {
        super.onPostExecute(repositoryPulls);

        if(listener != null) {
            listener.resfreshRepository(repositoryPulls);
        }
    }

    public interface OnRefreshRepositoryPullListener {
        void resfreshRepository(List<RepositoryPull> repositoryPulls);
    }
}
