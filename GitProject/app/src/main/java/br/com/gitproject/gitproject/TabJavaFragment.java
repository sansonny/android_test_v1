package br.com.gitproject.gitproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.gitproject.gitproject.Adapters.RepositoryListAdapter;
import br.com.gitproject.gitproject.Utils.DetectaConexao;
import br.com.gitproject.gitproject.Utils.EndlessScrollListener;
import br.com.gitproject.gitproject.models.RepositoryItem;
import br.com.gitproject.gitproject.models.RepositoryResult;
import br.com.gitproject.gitproject.tasks.RepositoryListTask;

/**
 * Created by rcastilho on 05/03/2018.
 */

public class TabJavaFragment extends Fragment {


    private static String LANGUAGE  = "";

    List<RepositoryItem> itens;
    ListView list;
    View rootView;
    RepositoryListAdapter adapter;
    SwipeRefreshLayout mySwipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            LANGUAGE = bundle.getString("q", "");
        }

        rootView = inflater.inflate(R.layout.tab_generic, container, false);
        mySwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);

        setScrollListenner(LANGUAGE);

        mySwipeRefreshLayout.setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    setScrollListenner(LANGUAGE);
                }
            }
        );
        return rootView;

    }

    public void loadNextDataFromApi(final int offset, String lang) {
        mySwipeRefreshLayout.setRefreshing(true);
        new RepositoryListTask(new RepositoryListTask.OnRefreshRepositoryListener() {
            @Override
            public void resfreshRepository(RepositoryResult repositoryResult) {
                if(repositoryResult!=null){
                    if(offset==1){
                        itens = repositoryResult.getItems();
                        adapter = new RepositoryListAdapter(getActivity(), R.layout.item_repository_list, itens);
                        list.setAdapter(adapter);

                    }
                    else{
                        itens = repositoryResult.getItems();
                        adapter.addAll(itens);
                    }
                }
                mySwipeRefreshLayout.setRefreshing(false);
            }
        }, lang, offset).execute();
    }

    public void setScrollListenner(final String language){
        if(new DetectaConexao(getContext()).existeConexao()) {
            if (list != null) {
                list.setOnScrollListener(null);
                list.setOnItemClickListener(null);
                list = null;
                adapter = null;
            }

            list = (ListView) rootView.findViewById(R.id.repositories_list);
            itens = new ArrayList<RepositoryItem>();

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    RepositoryItem item = (RepositoryItem) parent.getItemAtPosition(position);
                    Intent intent = new Intent(getContext(), RepositoryActivity.class);
                    intent.putExtra("owner", item.getOwner().getLogin());
                    intent.putExtra("project", item.getName());
                    startActivity(intent);
                }

            });

            list.setOnScrollListener(new EndlessScrollListener(10, 1) {
                @Override
                public boolean onLoadMore(int page, int totalItemsCount) {
                    loadNextDataFromApi(page, language);

                    return true;
                }
            });


            loadNextDataFromApi(1, language);
        }
        else{
            Intent intent = new Intent(getContext(), ErroConexaoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
