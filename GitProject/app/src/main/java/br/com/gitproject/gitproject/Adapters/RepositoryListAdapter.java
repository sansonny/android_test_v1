package br.com.gitproject.gitproject.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.gitproject.gitproject.R;
import br.com.gitproject.gitproject.Utils.CircleTransformation;
import br.com.gitproject.gitproject.models.RepositoryItem;

/**
 * Created by rcastilho on 05/07/2017.
 */

public class RepositoryListAdapter extends ArrayAdapter<RepositoryItem> {
    private List<RepositoryItem> items = new ArrayList<RepositoryItem>();
    private Context context;
    private int resource;
    private View view;




    public RepositoryListAdapter(Context context, int resource, List<RepositoryItem> items) {
        super(context, resource, items);

        this.items = items;
        this.resource = resource;
        this.context = context;
    }

    public static class ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView description;
        public TextView stars;
        public TextView forks;
        public TextView username;

    }


    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(resource, parent,false);

        RepositoryItem item = items.get(position);

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.ivAvatar);
            holder.name = (TextView) convertView.findViewById(R.id.tvRepName);
            holder.description = (TextView) convertView.findViewById(R.id.tvRepDescription);
            holder.stars = (TextView) convertView.findViewById(R.id.tvStars);
            holder.forks = (TextView) convertView.findViewById(R.id.tvForks);
            holder.username = (TextView) convertView.findViewById(R.id.tvLogin);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.name.setText(item.getName());
        holder.description.setText(item.getDescription()+"...");
        holder.stars.setText(String.valueOf(item.getStargazers_count()));
        holder.forks.setText(String.valueOf(item.getForks_count()));
        holder.username.setText(item.getOwner().getLogin());

        Picasso.with(context).load(item.getOwner().getAvatar_url()).transform(new CircleTransformation()).into(holder.image);

        return convertView;
    }







}