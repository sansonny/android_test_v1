package br.com.gitproject.gitproject.models;

/**
 * Created by rcastilho on 05/03/2018.
 */

public class RepositoryPull {
    private String state;
    private String title;
    private String body;
    private RepositoryOwner user;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public RepositoryOwner getUser() {
        return user;
    }

    public void setUser(RepositoryOwner user) {
        this.user = user;
    }
}
