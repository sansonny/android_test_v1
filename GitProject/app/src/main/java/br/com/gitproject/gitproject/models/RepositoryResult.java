package br.com.gitproject.gitproject.models;

import java.util.List;

/**
 * Created by rcastilho on 05/03/2018.
 */

public class RepositoryResult {
    private long total_count;
    private boolean incomplete_results;
    private List<RepositoryItem> items;

    public long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(long total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<RepositoryItem> getItems() {
        return items;
    }

    public void setItems(List<RepositoryItem> items) {
        this.items = items;
    }
}

