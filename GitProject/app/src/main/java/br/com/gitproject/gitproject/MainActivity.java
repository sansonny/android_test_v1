package br.com.gitproject.gitproject;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

public class MainActivity extends BaseActivity {


    private SectionsPagerAdapter mSectionsPagerAdapter;


    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checaConexao(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();


            switch(position) {
                case 0:
                    TabJavaFragment tabJavaFragment = new TabJavaFragment();
                    bundle.putString("q", "language:Java");
                    tabJavaFragment.setArguments(bundle);
                    return tabJavaFragment;

                case 1:
                    TabJavaScriptFragment tabJavaScriptFragment = new TabJavaScriptFragment();
                    bundle.putString("q", "language:Javascript");
                    tabJavaScriptFragment.setArguments(bundle);
                    return tabJavaScriptFragment;

                case 2:
                    TabNodeFragment tabNodeFragment = new TabNodeFragment();
                    bundle.putString("q", "Node js");
                    tabNodeFragment.setArguments(bundle);
                    return tabNodeFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }
    }
}
