package br.com.gitproject.gitproject.models;

/**
 * Created by rcastilho on 05/03/2018.
 */

public class RepositoryItem {
    private int id;
    private String name;
    private String full_name;
    private String description;
    private int stargazers_count;
    private int forks_count;
    private RepositoryOwner owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public int getForks_count() {
        return forks_count;
    }

    public void setForks_count(int forks_count) {
        this.forks_count = forks_count;
    }

    public RepositoryOwner getOwner() {
        return owner;
    }

    public void setOwner(RepositoryOwner owner) {
        this.owner = owner;
    }
}
