package br.com.gitproject.gitproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import br.com.gitproject.gitproject.Utils.DetectaConexao;


public class ErroConexaoActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_erro_conexao);
        setTitle("Erro!");

        Button refreshConnection = (Button) findViewById(R.id.btRefreshConnection);

        refreshConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new DetectaConexao(getApplicationContext()).existeConexao()){
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Sem conexão de internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
