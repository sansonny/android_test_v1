package br.com.gitproject.gitproject.tasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import br.com.gitproject.gitproject.BuildConfig;
import br.com.gitproject.gitproject.Utils.Parameters;
import br.com.gitproject.gitproject.models.RepositoryResult;
import br.com.gitproject.gitproject.services.GitService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rcastilho on 05/03/2018.
 */

public class RepositoryListTask extends AsyncTask<Void, Void, RepositoryResult> {

    private String lang;
    private int page;


    private OnRefreshRepositoryListener listener;

    public RepositoryListTask(OnRefreshRepositoryListener listener, String lang, int page) {
        this.listener = listener;
        this.lang = lang;
        this.page = page;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected RepositoryResult doInBackground(Void... voids){
        RepositoryResult repositoryResult = null;

        /*
        * Olá, como está? Deixei OkHttpclient para você que vai analisar meu código, ter feedback
        * das chamadas e retornos. Eu mesmo me preocupei quando ví que em alguns momentos a lista não chegava até o fim.
        * Isso acontece por causa que estou utilizando a versão não autenticada da api. Como não era um pré-requesito, não a autentiquei.
        * */

        OkHttpClient.Builder okhttpClienteBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        if(BuildConfig.DEBUG){
            okhttpClienteBuilder.addInterceptor(logging);
        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Parameters.URL_REPOSITORIES)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttpClienteBuilder.build());

        final Retrofit retrofit = builder.build();
        GitService service = retrofit.create(GitService.class);

        Map<String, String> data = new HashMap<>();
        data.put("q", lang);
        data.put("sort", "stars:"+lang);
        data.put("page", String.valueOf(page));

        Call<RepositoryResult> call = service.getRepositories(data);

        try {
            repositoryResult = call.execute().body();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return repositoryResult;
    }

    @Override
    protected void onPostExecute(RepositoryResult repositoryResult) {
        super.onPostExecute(repositoryResult);

        if(listener != null) {
            listener.resfreshRepository(repositoryResult);
        }
    }

    public interface OnRefreshRepositoryListener {
        void resfreshRepository(RepositoryResult repositoryResult);
    }
}
